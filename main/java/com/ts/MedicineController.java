package com.ts;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.management.AttributeNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.MedicineRepository;
import com.model.Medicine;


@RestController
public class MedicineController {
	
	@Autowired
	private MedicineRepository medicineRepository;
	
	@GetMapping("/medicines")
	public List<Medicine> getAllMedicines(){
		return medicineRepository.findAll();
	}
	
	@PostMapping("/medicines")
	public Medicine createMedicine(@RequestBody Medicine medicine) {
		return medicineRepository.save(medicine);
	}
	
	@GetMapping("/medicines/{id}")
	public Medicine getPatientById(@PathVariable Long id) throws AttributeNotFoundException {
		return medicineRepository.findById(id).orElse(null);
	}
	
	@PutMapping("/medicinesupdate/{medicine}")
	public Medicine updateMedicine(@PathVariable Medicine medicine) throws AttributeNotFoundException{
		return  medicineRepository.save(medicine);
	}
	
	@DeleteMapping("/medicinesdel/{id}")
	public ResponseEntity<Map<String,Boolean>> deleteMedicine(@PathVariable Long id) throws AttributeNotFoundException{
		
		Medicine medicine = medicineRepository.findById(id)
				.orElseThrow(() -> new AttributeNotFoundException("ABCD" + id));
		
		medicineRepository.delete(medicine);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
	
		

}