package com.ts;


import java.util.List;


import javax.management.AttributeNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AppointmentRepository;
import com.model.Appointment;

@RestController
public class AppointmentController {
	
	@Autowired
	private AppointmentRepository appointmentRepository;
	
	@GetMapping("/appointments")
	public List<Appointment> getAllAppointments(){
		return appointmentRepository.findAll();
	}
	
	@PostMapping("/appointments")
	public Appointment createAppointment(@RequestBody Appointment appointment) {
		return appointmentRepository.save(appointment);
	}
	
	@GetMapping("/appointments/{id}")
	public Appointment getAppointmentById(@PathVariable Long id) throws AttributeNotFoundException {
		return appointmentRepository.findById(id).orElse(null);
	}
	
	@DeleteMapping("/appointmentsdel/{id}")
	public void deleteAppointment(@PathVariable Long id) throws AttributeNotFoundException{
		
		appointmentRepository.deleteById(id);
	}
	
	

}
